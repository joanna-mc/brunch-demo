// Import a logger for easier debugging.
import debug from 'debug';
const log = debug('(log)');

const ENV = 'dev';

import json from "./json.json";

if (ENV !== 'production') {
    // Enable the logger.
    debug.enable('*');
    log('Logging is enabled! 🤗🤗');
    log('Quack Quack 🦆');

} else {
    debug.disable();
}

// TODO: Bundle JSON in a separate file & add script yaml -> json
log(json.user[0].name);

